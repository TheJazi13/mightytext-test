package net.mightytext.test;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void openPhotoCamera(View view) {
    	if (checkCameraHardware(this)) {
    		Intent intent = new Intent(this, PhotoCameraActivity.class);
    		startActivity(intent);
    	}
    }
    
    public void openVideoCamera(View view) {
    	if (checkCameraHardware(this)) {
    		Intent intent = new Intent(this, VideoCameraActivity.class);
    		startActivity(intent);
    	}
    }
    
    public void openAudioRecording(View view) {
    	Intent intent = new Intent(this, AudioRecordingActivity.class);
    	startActivity(intent);
    }
    
    public void openTextUpdate (View view) {
    	Intent intent = new Intent(this, TextUpdateActivity.class);
    	startActivity(intent);
    }
    
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        } else {
            return false;
        }
    }
}
