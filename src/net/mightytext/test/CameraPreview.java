package net.mightytext.test;

import java.io.IOException;
import java.lang.reflect.Method;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ZoomControls;

/**
 * @author Kyle Lucas
 */
public class CameraPreview extends SurfaceView implements
		SurfaceHolder.Callback {

	public static final String LOG_TAG = "MightyText";
	public static final int ORIENTATION_PORTRAIT = 1;

	protected SurfaceHolder myHolder;
	protected Camera myCamera;
	protected ZoomControls myZoomControls;
	protected int maxZoomLevel;
	protected int currentZoomLevel;

	/**
	 * @param context
	 * @param camera
	 *            the {@link Camera} to draw the preview from
	 * @param zc
	 *            the current {@link Activity}'s {@link zoomControls}
	 */
	@SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera, ZoomControls zc) {
		super(context);
		
		if (camera == null)
			try {
				myCamera = Camera.open();
			} catch (Exception e) {
				Log.e(LOG_TAG, e.getMessage());
			}
		else 
			myCamera = camera;
		
		
		myZoomControls = zc;

		myHolder = getHolder();
		myHolder.addCallback(this);

		// deprecated setting, but required on Android versions prior to 3.0
		myHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	/**
	 * Starts the facial detection.
	 */
	@SuppressLint({ "NewApi", "NewApi" })
	public void startFaceDetection() {

		/*
		 * Check if the current API being run is a high enough version for Face
		 * Detection If it is then start Face Detection
		 */
		if (android.os.Build.VERSION.SDK_INT >= 14) {
			Camera.Parameters params = myCamera.getParameters();

			if (params.getMaxNumDetectedFaces() > 0) {
				myCamera.startFaceDetection();
			}
		}
	}

	/**
	 * @see {@link android.view.SurfaceHolder.Callback}
	 */
	public void surfaceCreated(SurfaceHolder holder) {

		/* 
		 * Try to setup and start the Preview
		 * If it fails then log the error 
		 */
		try {

			/* */
			getOrientation();
			
			myCamera.setPreviewDisplay(holder);

			Camera.Parameters parameters = myCamera.getParameters();
			parameters.set("orientation", "portrait");
			myCamera.setParameters(parameters);

			myCamera.startPreview();
		} catch (IOException e) {
			Log.d(LOG_TAG, "Error setting camera preview: " + e.getMessage());
		}

		startFaceDetection();
	}

	/**
	 * Restarts the {@link android.hardware.Camera} preview.
	 */
	public void restartPreview() {
		myCamera.stopPreview();

		getOrientation();

		try {
			myCamera.setPreviewDisplay(myHolder);
		} catch (IOException e) {
			Log.e(LOG_TAG, e.getMessage());
		}

		myCamera.startPreview();

		startFaceDetection();
	}

	/**
	 * @see {@link android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view.SurfaceHolder)}
	 */
	public void surfaceDestroyed(SurfaceHolder holder) {
		try { 
			myCamera.stopPreview();
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage());
		}
	}

	/**
	 * @see {@link android.view.SurfaceHolder.Callback#surfaceChanged(android.view.SurfaceHolder, int, int, int)}
	 */
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

		/* 
		 * Check if the Holder object has a null Surface object
		 * If it does then stop the method
		 * If it doesn't then do nothing 
		 */
		if (myHolder.getSurface() == null) {
			return;
		}

		/* 
		 * Try to stop the current preview
		 * If it fails then log the error
		 */
		try {
			myCamera.stopPreview();
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage());
		}

		/* Check the current orientation and reorient if needed */
		getOrientation();

		/* Set the zoom system up */
		setZoom();

		/* 
		 * Try to set the Preview display, start the Preview, and start Face Detection
		 * If it fails then log the error 
		 */
		try {
			myCamera.setPreviewDisplay(myHolder);
			myCamera.startPreview();
			startFaceDetection();
		} catch (Exception e) {
			Log.d(LOG_TAG, "Error starting camera preview: " + e.getMessage());
		}
	}

	/**
	 * Controls the Zoom of the current {@link android.hardware.Camera} object
	 */
	protected void setZoom() {

		/* Get the current Camera object's Parameters */
		Camera.Parameters params = myCamera.getParameters();

		/* 
		 * Check if smooth zoom is supported
		 * If smooth zoom is supported then get the max zoom, enable zoom in and out then start the smooth zoom listeners
		 * If it is not check to see if zoom is supported
		 * If zoom is supported then get the max zoom, enable zoom in and out then start the standard zoom listeners
		 * If neither are supported then hide the zoom buttons
		 */
		if (params.isSmoothZoomSupported()) {
			
			/* Get the max camera zoom parameter then set zoom in and out to true */
			maxZoomLevel = params.getMaxZoom();
			myZoomControls.setIsZoomInEnabled(true);
			myZoomControls.setIsZoomOutEnabled(true);
			
			/* Start the listener for smooth zoom in */
			smoothZoomInListener();
			
			/* Start the listener for smooth zoom out */
			smoothZoomOutListener();

		} else if (params.isZoomSupported()) {
			
			/* Get the max camera zoom parameter then set zoom in and out to true */
			maxZoomLevel = params.getMaxZoom();
			myZoomControls.setIsZoomInEnabled(true);
			myZoomControls.setIsZoomOutEnabled(true);
			
			/* Start the listener for standard zoom in */
			standardZoomInListener();
			
			/* Start the listener for standard zoom out */
			standardZoomOutListener();
		} else
			myZoomControls.setVisibility(View.GONE);
	}

	/**
	 * Changes the display orientation of the
	 */
	protected void getOrientation() {
		/*
		 * Check the current orientation
		 * If it is in Portrait mode then set the Display orientation to 90 degrees
		 * If it is in Landscape mode then set the Display orientation to 0 degrees
		 */
		if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
			setDisplayOrientation(myCamera, 90);
		} else {
			setDisplayOrientation(myCamera, 0);
		}
	}

	/**
	 * 
	 */
	protected void smoothZoomInListener() {
		
		/* Set the listener for the zoom in part of the zoom controls */
		myZoomControls.setOnZoomInClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				/* 
				 * Check if the current zoom level is less than the maximum zoom level
				 * If it is then allow the camera to zoom in
				 * If it is not then do nothing 
				 */
				if (currentZoomLevel < maxZoomLevel) {
					
					currentZoomLevel++;
					
					/*
					 * Try to smooth zoom in the camera to the currentZoomLevel
					 * If it fails then log the error
					 */
					try {
						myCamera.startSmoothZoom(currentZoomLevel);
					} catch (RuntimeException e) {
						Log.e(LOG_TAG, e.getMessage());
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	protected void smoothZoomOutListener() {

		/* Set the listener for the zoom out part of the zoom controls */
		myZoomControls.setOnZoomOutClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				/* 
				 * Check if the current zoom level is greater than the maximum zoom level
				 * If it is then allow the camera to zoom out
				 * If it is not then do nothing 
				 */
				if (currentZoomLevel > 0) {
					
					currentZoomLevel--;
					
					/*
					 * Try to smooth zoom out the camera to the currentZoomLevel
					 * If it fails then log the error
					 */
					try {
						myCamera.startSmoothZoom(currentZoomLevel);
					} catch (RuntimeException e) {
						Log.e(LOG_TAG, e.getMessage());
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	protected void standardZoomInListener() {
		
		/* Set the listener for the zoom out part of the zoom controls */
		myZoomControls.setOnZoomInClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				/* 
				 * Check if the current zoom level is less than the maximum zoom level
				 * If it is then allow the camera to zoom in
				 * If it is not then do nothing 
				 */
				if (currentZoomLevel < maxZoomLevel) {
					
					currentZoomLevel++;
					
					/* Get the current parameter then set the new zoom level to the currentZoomLevel then set the new parameters */
					Camera.Parameters params = myCamera.getParameters();
					params.setZoom(currentZoomLevel);
					myCamera.setParameters(params);
				}
			}
		});

	}

	/**
	 * 
	 */
	protected void standardZoomOutListener() {
		
		/* Set the listener for the zoom out part of the zoom controls */
		myZoomControls.setOnZoomOutClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				/* 
				 * Check if the current zoom level is greater than the maximum zoom level
				 * If it is then allow the camera to zoom out
				 * If it is not then do nothing 
				 */
				if (currentZoomLevel > 0) {
					
					currentZoomLevel--;
					
					/* Get the current parameter then set the new zoom level to the currentZoomLevel then set the new parameters */
					Camera.Parameters params = myCamera.getParameters();
					params.setZoom(currentZoomLevel);
					myCamera.setParameters(params);
				}
			}
		});

	}

	/**
	 * @param camera
	 *            the {@link android.hardware.Camera} to set the preview
	 *            orientation for
	 * @param angle
	 *            the angle to set the preview to
	 */
	protected void setDisplayOrientation(Camera camera, int angle) {
		
		Method downPolymorphic;
		
		/* 
		 * Try to create the new method downPolymorphic from the setDisplayOrientation method in android.hardware.Camera 
		 * If it fails then log the error
		 */
		try {
			downPolymorphic = camera.getClass().getMethod(
					"setDisplayOrientation", new Class[] { int.class });
			
			/* 
			 * If the previous line worked then run the new method with the giver parameters
			 * If it did not then do nothing 
			 */
			if (downPolymorphic != null)
				downPolymorphic.invoke(camera, new Object[] { angle });
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage());
		}
	}


}