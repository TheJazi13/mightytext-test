package net.mightytext.test;

import java.io.IOException;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ZoomControls;

/**
 * @author Kyle Lucas
 */
public class VideoCameraActivity extends CameraCapture {

	protected boolean isRecording = false;
	protected MediaRecorder myMediaRecorder;
	protected Button captureButton;


	/**
	 * @param Bundle
	 *            SavedInstanceState
	 * @see {@link android.app.Activity#onCreate(android.os.Bundle)}
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_open_photo_camera);
		captureButton = (Button) findViewById(R.id.button_capture);
		
		/* Set the camera to the current Camera instance */
		myCamera = getCameraInstance();

		ZoomControls zoomControls = (ZoomControls) findViewById(R.id.CAMERA_ZOOM_CONTROLS);

		myPreview = new CameraPreview(this, myCamera, zoomControls);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(myPreview);

		setCaptureListener();
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	protected void onPause() {
		super.onPause();
		releaseMediaRecorder(); // if you are using MediaRecorder, release it
								// first
		releaseCamera(); // release the camera immediately on pause event
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		releaseCamera();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseCamera();
	}

	/**
	 * @return
	 */
	private boolean prepareVideoRecorder() {

		myMediaRecorder = new MediaRecorder();

		// Step 1: Unlock and set camera to MediaRecorder
		myCamera.stopPreview();
		myCamera.unlock();
		myMediaRecorder.setCamera(myCamera);

		// Step 2: Set sources
		myMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		myMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

		// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
		myMediaRecorder.setProfile(CamcorderProfile
				.get(CamcorderProfile.QUALITY_HIGH));

		// Step 4: Set output file
		myMediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO,
				false).toString());

		// Step 5: Set the preview output
		myMediaRecorder.setPreviewDisplay(myPreview.getHolder().getSurface());

		// Step 6: Prepare configured MediaRecorder
		try {
			myMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d("DEBUG", "IllegalStateException preparing MediaRecorder: "
					+ e.getMessage());
			releaseMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d("DEBUG",
					"IOException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 */
	public void setCaptureListener() {
		captureButton.setOnClickListener(new View.OnClickListener() {
			/* (non-Javadoc)
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {
				Camera.Parameters params = myCamera.getParameters();
				
				if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
					params.setRotation(90);
					myCamera.setParameters(params);
				} else {
					params.setRotation(0);
					myCamera.setParameters(params);
				}
				
				if (isRecording) {
					// stop recording and release camera
					myMediaRecorder.stop(); // stop the recording
					releaseMediaRecorder(); // release the MediaRecorder object
					myCamera.lock(); // take camera access back from
										// MediaRecorder

					// inform the user that recording has stopped
					captureButton.setText("Capture");
					isRecording = false;
					myPreview.restartPreview();
				} else {
					// initialize video camera
					if (prepareVideoRecorder()) {
						// Camera is available and unlocked, MediaRecorder is
						// prepared,
						// now you can start recording
						myMediaRecorder.start();

						// inform the user that recording has started
						captureButton.setText("Stop");
						isRecording = true;
					} else {
						// prepare didn't work, release the camera
						releaseMediaRecorder();
						// inform user
					}
				}
			}
		});
	}
	
	/**
	 * 
	 */
	private void releaseMediaRecorder() {
		if (myMediaRecorder != null) {
			myMediaRecorder.reset(); // clear recorder configuration
			myMediaRecorder.release(); // release the recorder object
			myMediaRecorder = null;
			myCamera.setPreviewCallback(null);
			myCamera.lock(); // lock camera for later use
		}
	}
	
	/**
	 * 
	 */
	public void releaseCamera() {
		if (myCamera != null) {
			isRecording = false;
			myCamera.stopPreview();
			myCamera.setPreviewCallback(null);
			myCamera.release(); // release the camera for other applications
			myCamera = null;
		}
	}
}
