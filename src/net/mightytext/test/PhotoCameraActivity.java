package net.mightytext.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.PictureCallback;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ZoomControls;

/**
 * @author Kyle Lucas
 */
public class PhotoCameraActivity extends CameraCapture {

	protected Bitmap myBitmap;

	/**
	 * @see {@link android.app.Activity#onCreate}
	 */
	//@SuppressLint("NewApi")
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		Debug.startMethodTracing("calc");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_open_photo_camera);
		Button captureButton = (Button) findViewById(R.id.button_capture);

		/* Set the camera to the current Camera instance */
		myCamera = getCameraInstance();
		
		if (android.os.Build.VERSION.SDK_INT >= 14)
			myCamera.setFaceDetectionListener(new MyFaceDetectionListener());
		
		ZoomControls zoomControls = (ZoomControls) findViewById(R.id.CAMERA_ZOOM_CONTROLS);
		
		myPreview = new CameraPreview(this, myCamera, zoomControls);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(myPreview);

		final PictureCallback mPicture = new PictureCallback() {

			/**
			 * @see android.hardware.Camera.PictureCallback#onPictureTaken(byte[], android.hardware.Camera)
			 */
			public void onPictureTaken(byte[] data, Camera camera) {
				
				/* New ASyncTask to save original file in the background */
				(new AsyncTask<byte[], String, String>() {

					@Override
					protected String doInBackground(byte[]... params) {
						
						/* Save the picture to the SD card */
						savePictureToSDCard(params[0]);
						return null;
					}

					private void savePictureToSDCard(byte[] bs) {
						
						/* Get the file to save the data captured by the camera to */
						File photo = getOutputMediaFile(MEDIA_TYPE_IMAGE, false);

						/* 
						 * Try to write the data to the file with a FileOutputStream
						 * If it fails then log the error
						 */
						try {
							FileOutputStream fos = new FileOutputStream(
									photo.getPath());
							fos.write(bs);
							fos.close();
							fos = null;

						} catch (IOException e) {
							Log.e(LOG_TAG, e.getMessage());
						}
					}
				}).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

				/* New ASyncTask to save the compressed temporary file in the background */
				(new AsyncTask<byte[], String, String>() {

					/**
					 * @see android.os.AsyncTask#doInBackground(Params[])
					 */
					@Override
					protected String doInBackground(byte[]... params) {
						
						/* Save the picture to the SD card */
						savePictureToSDCard(params[0]);
						return null;
					}

					/**
					 * @param bs the byte array containing the data captured by the camera
					 */
					private void savePictureToSDCard(byte[] bs) {
						
						/* Get the file to save the data captured from the camera to */
						File photo = getOutputMediaFile(MEDIA_TYPE_IMAGE, true);
						
						/*
						 * Try to write the data to a file
						 * If it fails then log the error
						 */
						try {

							if (true) {	
								/* Load the data the camera captured as a Bitmap */
								myBitmap = BitmapFactory.decodeByteArray(bs, 0,
										bs.length);

								/* Generate a FileOutputStream for the file to be saved to */
								FileOutputStream fos = new FileOutputStream(
										photo);
								ByteArrayOutputStream bytes = new ByteArrayOutputStream();
								
								/* Compress the bitmap to reduce size */
								myBitmap.compress(Bitmap.CompressFormat.JPEG,
										30, bytes);
								
								/* Write the bitmap to the file then close and get rid of the FileOutputStream */
								fos.write(bytes.toByteArray());
								
								/* Clear the used objects from memory */
								myBitmap.recycle();
								myBitmap = null;
								fos.close();
								fos = null;
							}

						} catch (FileNotFoundException e) {
							Log.e(LOG_TAG, e.getMessage());
						} catch (IOException e) {
							Log.e(LOG_TAG, e.getMessage());
						}
					}
				}).execute(data);

				/* Restart the preview so the user can take another picture */
				myPreview.restartPreview();
			}
		};

		captureButton.setOnClickListener(new View.OnClickListener() {
			Camera.Parameters params = myCamera.getParameters();

			/**
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {
				/* Check the orientation of the phone and properly orient the preview before capture */
				if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
					params.setRotation(90);
					myCamera.setParameters(params);
				} else {
					params.setRotation(0);
					myCamera.setParameters(params);
				}
				
				/* Capture the picture */
				myCamera.takePicture(null, null, mPicture);
			}
		});
	}

	/**
	 * @see {@link android.app.Activity#onPause()}
	 */
	@Override
	protected void onPause() {
		super.onPause();
		releaseCamera();
	}

	/**
	 * @see {@link android.app.Activity#onBackPressed()}
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		releaseCamera();
	}

	/**
	 * @see {@link android.app.Activity#onDestroy()}
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseCamera();
		
		Debug.stopMethodTracing();
	}

	/**
	 * @author Kyle
	 *
	 */
	class MyFaceDetectionListener implements Camera.FaceDetectionListener {

		/**
		 * @see {@link android.hardware.Camera.FaceDetectionListener#onFaceDetection(android.hardware.Camera.Face[], android.hardware.Camera)}
		 */
		//@SuppressLint({ "NewApi", "NewApi" })
		@SuppressLint({ "NewApi", "NewApi" })
		public void onFaceDetection(Face[] faces, Camera camera) {
			if (faces.length > 0) {
				Log.d("FaceDetection", "face detected: " + faces.length
						+ " Face 1 Location X: " + faces[0].rect.centerX()
						+ "Y: " + faces[0].rect.centerY());
			}
		}
	}

}