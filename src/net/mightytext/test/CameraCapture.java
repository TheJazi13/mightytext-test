package net.mightytext.test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

/**
 * @author Kyle Lucas
 */
public class CameraCapture extends Activity {

	public static final int MEDIA_TYPE_VIDEO = 2;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final String LOG_TAG = "MightyText";
	public static final int ORIENTATION_PORTRAIT = 1;
	public static final int ORIENTATION_LANDSCAPE = 2;

	protected Camera myCamera;
	protected CameraPreview myPreview;

	/**
	 * @return the current {@link android.hardware.Camera} instance
	 */
	public static Camera getCameraInstance() {

		/* Create an empty Camera object */
		Camera camera = null;

		/*
		 * Try to set the camera object to the current hardware Camera. If this
		 * fails then log the exception.
		 */
		try {
			camera = Camera.open();
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage());
		}

		return camera;
	}

	/**
	 * @param type
	 *            the type of file to output, 1 for Photo, 2 for Video
	 * @param temp
	 *            if the file is to be a temporary file or not
	 * @return
	 */
	public File getOutputMediaFile(int type, boolean temp) {

		String dirName;

		/*
		 * Set the directory name on the basis of if it is to be a temporary
		 * file or not
		 */
		if (temp)
			dirName = "MightyTextTemp";
		else
			dirName = "MightyText";

		/* Create the file directory to be saved to */
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
				dirName);

		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}

		/* Add a timestamp to the newly created filename */
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSS")
				.format(new Date());
		File mediaFile;

		/*
		 * Create the file to be saved to If it is to be an Image file give it
		 * the extension '.jpg' If it is to be a Video file give it the
		 * extension '.3gp'
		 */
		if (type == MEDIA_TYPE_IMAGE)
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		else if (type == MEDIA_TYPE_VIDEO)
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".3gp");
		else
			return null;

		return mediaFile;

	}

	/**
	 * Properly releases the current {@link android.hardware.Camera} object
	 */
	public void releaseCamera() {
		if (myCamera != null) {
			myCamera.release();
			myCamera = null;
		}
	}

	/**
	 * @param view
	 */
	public void onFlashToggleClicked(View view) {
		
		/* Get the current Camera object's Parameters */
		Camera.Parameters params = myCamera.getParameters();
		boolean on = ((ToggleButton) view).isChecked();
	
		/*
		 * Check the state of the toggle button
		 * If it is checked then turn flash on
		 * If it is off then turn flash off */
		if (on) {
			params.setFlashMode("on");
		} else {
			params.setFlashMode("off");
		}
	
		/* Set the new parameters */
		myCamera.setParameters(params);
	}

	/**
	 * Default Constructor for the {@link CameraCapture}
	 */
	public CameraCapture() {
		super();
	}

}