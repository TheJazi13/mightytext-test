package net.mightytext.test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

/**
 * @author Kyle
 */
public class AudioRecordingActivity extends Activity {

	public static final String LOG_TAG = "MightyText";
	
	protected MediaRecorder myMediaRecorder;
	protected boolean isRecording = false;
	protected Button recordButton;
	

	/**
	 * @see {@link android.app.Activity#onCreate}
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_audio_recording);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        recordButton = (Button) findViewById(R.id.record_button);

        recordButton.setOnClickListener(
    		    new View.OnClickListener() {
    		        
    		        public void onClick(View v) {
    		            if (isRecording) {
    		                myMediaRecorder.stop();
    		                myMediaRecorder.release();
    		                myMediaRecorder = null;

    		                setRecordButtonText("Record");
    		                isRecording = false;
    		            } else {
    		            	myMediaRecorder = new MediaRecorder();

    		                if (prepareMediaRecorder()) {
    		                    myMediaRecorder.start();
    		                    setRecordButtonText("Stop");
    		                    isRecording = true;
    		                } else {
    		                    myMediaRecorder.release();
    		                }
    		            }
    		        }
    		    }
    		);  
    }

    /**
     * @param string the text to set the {@link R.id#record_button} to display
     */
    protected void setRecordButtonText(String string) {
		recordButton.setText(string);
	}

	/**
	 * @return if the {@link android.media.MediaRecorder} was properly prepared
	 */
	private boolean prepareMediaRecorder() {
       	
		myMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		myMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		myMediaRecorder.setOutputFile(getFilePath());
		myMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		try {
			myMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.e(LOG_TAG, e.getMessage());
			return false;
		} catch (IOException e) {
			Log.e(LOG_TAG, e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * @return the path to the file to be output
	 */
	private String getFilePath() {
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_MUSIC), "MightyText");

	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MightyText", "failed to create directory");
	            return null;
	        }
	    }
	
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        	timeStamp + ".3gp");
	    
	    return mediaFile.getAbsolutePath();
	}

	/**
	 * @see {@link android.app.Activity#onCreateOptionsMenu}
	 */
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_open_audio_recording, menu);
        return true;
    }

	/**
	 * @see {@link android.app.Activity#onPause()}
	 */
	@Override
	public void onPause() {
	        super.onPause();
	        if (myMediaRecorder != null) {
	            myMediaRecorder.release();
	            myMediaRecorder = null;
	        }

	    }
	 
    /**
     * @see {@link android.app.Activity#onOptionsItemSelected}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
